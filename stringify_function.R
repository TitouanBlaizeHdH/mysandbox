function_lines <- readLines("utils/func_faf.R")

for (line in function_lines) {
  print(line)
}

# Créer à la main un fichier fake file ou on copie coller cette sortie
# Ainsi qu'un true file où on aura le format attendu
fake_lines <- readLines("fakefile")
con <- file("truefile.R", "w")

for (line in fake_lines) {
  clean_line <- print(str_split(line, " ", n = 2))[[1]][2]
  writeLines(paste0(clean_line, ","), con)
}

close(con)

# On a ainsi dans truefile le bon format à copie coller dans sa fonction d'installation

print(str_split(fake_lines[3], " ", n = 2)[[1]][2])


